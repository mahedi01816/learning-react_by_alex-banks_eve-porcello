/*const getFakePerson = async () => {
    let res = await fetch("");
    let {results} = res.json();
    console.log(results);
};

getFakePerson();*/

const getFakePerson = async () => {
    try {
        let res = await fetch("");
        let {results} = res.json();
        console.log(results);
    } catch (e) {
        console.error(e);
    }
};

getFakePerson();