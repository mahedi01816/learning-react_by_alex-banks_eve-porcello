console.log(lastName + ", " + firstName + " " + middleName);
console.log(`${lastName}, ${firstName} ${middleName}`);

const email = `
Hello ${firstName},

Thanks for ordering ${qty} tickets to ${event}.

Order Details
${firstName} ${middleName} ${lastName}
${qty} x $${price} = $${qty * price} to ${event}

You can pick your tickets up 30 minutes before the show.

Thanks,

${ticketAgent}
`;

document.body.innerHTML = `
<section>
<header>
<h1>The React Blog</h1>
</header>
<article>
<h2>${article.title}</h2>
${article.body}
</article>
<footer>
<p>copyright ${new Date().getUTCFullYear()} | The React Blog</p>
</footer>
</section>
`;