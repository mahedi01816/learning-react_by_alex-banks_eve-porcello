const createCompliment = function (firstName, message) {
    return `${firstName}: ${message}`;
};

console.log(createCompliment("Molly", "You're so cool"));