/*const lordify = function (firstName) {
    return `${firstName} of Canterbury`;
};

console.log(lordify("Dale"));
console.log(lordify("Gail"));*/

/*const lordify = firstName => `${firstName} of Canterbury`;
console.log(lordify("Dale"));*/

/*const lordify = function (firstName, land) {
    return `${firstName} of ${land}`;
};*/

/*const lordify = (firstName, land) => `${firstName} of ${land}`;

console.log(lordify("Don", "Piscataway"));
console.log(lordify("Todd", "Schenectady"));*/

const lordify = (firstName, land) => {
    if (!firstName) {
        throw new Error("A firstName is required to lordify");
    }

    if (!land) {
        throw new Error("A lord must have a land");
    }

    return `${firstName} of ${land}`;
};

console.log(lordify("Kelly", "Sonoma"));
console.log(lordify("Dave"));